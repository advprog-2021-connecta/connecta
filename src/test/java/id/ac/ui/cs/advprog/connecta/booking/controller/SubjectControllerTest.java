package id.ac.ui.cs.advprog.connecta.booking.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.booking.service.SubjectService;
import id.ac.ui.cs.advprog.connecta.security.util.JwtUtil;
import id.ac.ui.cs.advprog.connecta.user.repository.model.ConnectaUserDetails;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SubjectController.class)
class SubjectControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SubjectService subjectSvc;

    @MockBean
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    private User user;
    private Subject subject;
    private ConnectaUserDetails userDetails;

    @BeforeEach
    public void setUp() {
        user = new User("Dummy McDummy", "test@test.com", "test123");
        user.setId(1L);
        subject = new Subject("AP", "Advanced Programming");
        userDetails = new ConnectaUserDetails(user);
    }

    @Test
    @WithMockUser
    void testFindAllSubjects_OK() throws Exception {
        ArrayList<Subject> subjects = new ArrayList<>();
        subjects.add(subject);
        when(subjectSvc.findAllEligibleSubjects("test@test.com")).thenReturn(subjects);

        String token = "Bearer " + JwtUtil.generateToken(userDetails);

        mockMvc.perform(
            get("/api/subjects")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", token)
            )
            .andExpect(handler().methodName("findAllSubjects"))
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void testCreateEvent_OK() throws Exception {
        ArrayList<Subject> subjects = new ArrayList<>();
        subjects.add(subject);
        when(subjectSvc.createSubject(any(Subject.class))).thenReturn(subject);

        String token = "Bearer " + JwtUtil.generateToken(userDetails);

        mockMvc.perform(
            post("/api/subjects")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"title\": \"AP\", \"description\": \" Advanced Programming\"}".getBytes())
                .header("Authorization", token)
            )
            .andExpect(handler().methodName("createSubject"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.title").value(subject.getTitle()));
    }

    @Test
    @WithMockUser
    void testEnrollToSubject_OK() throws Exception {
        SubjectService mockSubjectSvc = mock(SubjectService.class);
        doNothing().when(mockSubjectSvc).enrollToSubject(subject.getId(), user.getEmail());

        String token = "Bearer " + JwtUtil.generateToken(userDetails);

        mockMvc.perform(
            put("/api/subjects/1/enroll")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", token)
            )
            .andExpect(handler().methodName("enrollToSubject"))
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void testUnenrollFromSubject_OK() throws Exception {
        SubjectService mockSubjectSvc = mock(SubjectService.class);
        doNothing().when(mockSubjectSvc).unenrollFromSubject(subject.getId());

        String token = "Bearer " + JwtUtil.generateToken(userDetails);

        mockMvc.perform(
            put("/api/subjects/1/unenroll")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", token)
            )
            .andExpect(handler().methodName("unenrollFromSubject"))
            .andExpect(status().isOk());
    }
}

package id.ac.ui.cs.advprog.connecta.user.controller;

import id.ac.ui.cs.advprog.connecta.Util;
import id.ac.ui.cs.advprog.connecta.user.repository.model.ConnectaUserDetails;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = AuthenticationController.class)
class AuthenticationControllerTest {

    @InjectMocks
    AuthenticationController authenticationController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;
    
    @MockBean
    private AuthenticationManager authenticationManager;

    private UserDetails userDetails;
    private User user;

    @BeforeEach
    public void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
        userDetails = new ConnectaUserDetails(user.getEmail());
    }

    @Test
    void testCreateJwtTokenSuccess() throws Exception {
        Object bodyContent = new Object() {
            public final String email = user.getEmail();
            public final String password = user.getPassword();
        };

        when(authenticationManager.authenticate(any())).thenReturn(null);
        when(userDetailsService.loadUserByUsername(any())).thenReturn(userDetails);

        MvcResult result = mockMvc.perform(
                    post("/api/auth/authenticate")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Util.mapToJson(bodyContent))
                )
                .andExpect(handler().methodName("createAuthenticationToken"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.jwt").isString())
                .andReturn();
    }

    @Test
    void testCreateJwtTokenFailed() throws Exception {
        Object bodyContent = new Object() {
            public final String email = user.getEmail();
            public final String password = user.getPassword();
        };

        when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);

        MvcResult result = mockMvc.perform(
                    post("/api/auth/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapToJson(bodyContent))
                )
                .andExpect(handler().methodName("createAuthenticationToken"))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }
}

package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.booking.repository.SubjectRepository;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.UserSubjectRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserSubjectServiceImplTest {

    @InjectMocks
    private UserSubjectService userSubjectSvc = new UserSubjectServiceImpl();

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserSubjectRepository userSubjectRepository;
    
    @Mock
    private SubjectRepository subjectRepository;

    private User user;

    private UserSubject userSubject;

    private Subject subject = new Subject("AP", "Advanced Programming");

    @BeforeEach
    public void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
        userSubject = new UserSubject(1L, false, user, subject);
    }

    @Test
    void updateUserSubject_OK() {
        UserSubject req = userSubject;
        req.setIsTeachingAssistant(true);
        when(userSubjectRepository.findById(1L)).thenReturn(Optional.of(userSubject));
        UserSubject us =  userSubjectSvc.updateUserSubject(req);
        assertEquals(true, us.getIsTeachingAssistant());
    }

    @Test
    void updateUserSubject_UserSubjectNotFound() {
        UserSubject req = userSubject;
        req.setIsTeachingAssistant(true);
        when(userSubjectRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        UserSubject us =  userSubjectSvc.updateUserSubject(req);
        assertEquals(null, us);
    }

    @Test
    void findAllUserSubjectByUserId() {
        ArrayList<UserSubject> us = new ArrayList<>();
        us.add(userSubject);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(userSubjectRepository.findByUser(user)).thenReturn(us);

        Iterable<UserSubject> res = userSubjectSvc.findAllUserSubjectByUserId(1L);
        assertEquals(us.size(), ((Collection<?>) res).size());
        assertEquals(us.get(0), ((ArrayList<?>) res).get(0));
    }

    @Test
    void findAllUserSubjectBySubjectId() {
        ArrayList<UserSubject> us = new ArrayList<>();
        us.add(userSubject);
        when(subjectRepository.findById(anyLong())).thenReturn(Optional.of(subject));
        when(userSubjectRepository.findBySubject(subject)).thenReturn(us);

        Iterable<UserSubject> res = userSubjectSvc.findAllUserSubjectBySubjectId(1L);
        assertEquals(us.size(), ((Collection<?>) res).size());
        assertEquals(us.get(0), ((List<?>) res).get(0));
    }
}
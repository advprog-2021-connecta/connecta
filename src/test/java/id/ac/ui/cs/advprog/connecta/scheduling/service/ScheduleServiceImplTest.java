package id.ac.ui.cs.advprog.connecta.scheduling.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.EventRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.ScheduleRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ScheduleServiceImplTest {
    
    @InjectMocks
    private ScheduleService scheduleService = new ScheduleServiceImpl();

    @Mock
    private ScheduleRepository scheduleRepository;
    
    @Mock
    private UserRepository userRepository;

    @Mock
    private EventRepository eventRepository;

    private User user;
    private Subject subject;
    private Event event;
    private Schedule schedule;

    @BeforeEach
    public void setUp() {
        user = new User("Testy McTesty", "test@test.com", "test123");
        subject = new Subject("SubjectTitle", "SubjectDesc");
        event = new Event("EventTitle", subject, user, LocalDate.now(), LocalDate.now(),"https://meet.google.com/gud-ayae-ced?authuser=0&pli=1");
        schedule = new Schedule(event, event.getStartDate(), LocalTime.now().plusMinutes(10), LocalTime.now().plusMinutes(20));
    }

    @Test
    void createSchedule() {
        Schedule resultSchedule = scheduleService.createSchedule(schedule);
        assertEquals(schedule, resultSchedule);
    }

    @Test
    void getSchedulesOfEvent() {
        event.setSchedule(Arrays.asList(schedule));
        when(eventRepository.findEventById(1L)).thenReturn(event);

        List<Schedule> schedulesOfEvent = scheduleService.getSchedulesOfEvent(1L);

        boolean scheduleExists = false;

        for (Schedule scheduleInList: schedulesOfEvent) {
            if (scheduleInList.equals(schedule)) {
                scheduleExists = true;
            }
        }

        assertTrue(scheduleExists);
    }

    @Test
    void getAllSchedule() {
        when(scheduleRepository.findAll()).thenReturn(Arrays.asList(schedule));

        List<Schedule> allSchedule = scheduleService.getAllSchedule();

        boolean scheduleExists = false;

        for (Schedule scheduleInList: allSchedule) {
            if (scheduleInList.equals(schedule)) {
                scheduleExists = true;
            }
        }

        assertTrue(scheduleExists);
    }

    @Test
    void getScheduleById() {
        when(scheduleRepository.findScheduleById(1L)).thenReturn(schedule);

        Schedule resultSchedule = scheduleService.getScheduleById(1L);

        assertEquals(schedule, resultSchedule);
    }

}

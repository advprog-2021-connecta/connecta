package id.ac.ui.cs.advprog.connecta.user.repository.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AuthenticationRequestTest {
    @Test
    void createAuthenticationRequest() {
        String email = "dummy@dummy.com";
        String password = "dummy_password";
        AuthenticationRequest authReq = new AuthenticationRequest(email, password);
        assertEquals(email, authReq.getEmail());
        assertEquals(password, authReq.getPassword());
    }
}
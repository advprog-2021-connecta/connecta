package id.ac.ui.cs.advprog.connecta.user.repository.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.jupiter.api.Assertions.*;

class ConnectaUserDetailsTest {

    private User user;
    private UserDetails userDetails;

    @BeforeEach
    void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
        userDetails = new ConnectaUserDetails(user);
    }

    @Test
    void createWithNoArgsConstructor() {
        UserDetails newUserDetails = new ConnectaUserDetails();

        assertNotNull(newUserDetails);
    }

    @Test
    void getAuthorities() {
        assertTrue(userDetails.getAuthorities().isEmpty());
    }

    @Test
    void getAdminAuthorities() {
        user.setIsAdmin(true);
        userDetails = new ConnectaUserDetails(user);

        assertEquals(1, userDetails.getAuthorities().size());
    }

    @Test
    void getPassword() {
        assertEquals(user.getPassword(), userDetails.getPassword());
    }

    @Test
    void getUsername() {
        assertEquals(user.getEmail(), userDetails.getUsername());
    }

    @Test
    void getDefaultValue() {
        assertEquals(true, userDetails.isAccountNonExpired());
        assertEquals(true, userDetails.isAccountNonLocked());
        assertEquals(true, userDetails.isCredentialsNonExpired());
        assertEquals(true, userDetails.isEnabled());
    }

}
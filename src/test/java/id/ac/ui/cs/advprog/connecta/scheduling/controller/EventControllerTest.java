package id.ac.ui.cs.advprog.connecta.scheduling.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import id.ac.ui.cs.advprog.connecta.Util;
import id.ac.ui.cs.advprog.connecta.booking.repository.SubjectRepository;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.booking.service.SubjectService;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.ScheduleRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.scheduling.service.EventService;
import id.ac.ui.cs.advprog.connecta.scheduling.service.ScheduleService;
import id.ac.ui.cs.advprog.connecta.scheduling.service.EventService;
import id.ac.ui.cs.advprog.connecta.security.util.JwtUtil;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.ConnectaUserDetails;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import net.minidev.json.JSONObject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = EventController.class)
class EventControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    @MockBean
    private ScheduleService scheduleService;

    @MockBean
    private UserService userService;

    @MockBean
    private SubjectService subjectService;

    @MockBean
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    private User user;
    private Subject subject;
    private Event event;
    private Schedule schedule;
    private ConnectaUserDetails userDetails;

    @BeforeEach
    public void setUp() {
        user = new User("Testy McTesty", "test@test.com", "test123");
        user.setId(1L);
        subject = new Subject("SubjectTitle", "SubjectDesc");
        subject.setId(1L);
        event = new Event("EventTitle", subject, user, LocalDate.now(), LocalDate.now(),"https://meet.google.com/gud-ayae-ced?authuser=0&pli=1");
        schedule = new Schedule(event, LocalDate.now(), LocalTime.now().plusMinutes(10), LocalTime.now().plusMinutes(20));
        userDetails = new ConnectaUserDetails(user);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createEvent() throws Exception {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        Object bodyContent = new Object() {
            public final String title = event.getTitle();
            public final Long subjectId = 1L;
            public final String startDate = event.getStartDate().format(dateFormatter);
            public final String endDate = event.getEndDate().format(dateFormatter);
            public final String link=event.getLink();
        };

        when(eventService.createEvent(event)).thenReturn(event);
        when(subjectService.getSubjectById(1L)).thenReturn(subject);
        when(userService.getUserByEmail(user.getEmail())).thenReturn(user);

        String token = "Bearer " + JwtUtil.generateToken(userDetails);

        mockMvc.perform(
            post("/api/scheduling/event")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Util.mapToJson(bodyContent))
                .header("Authorization", token)
            )
            .andExpect(handler().methodName("createEvent"))
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void getEvent() throws Exception {
        when(eventService.getEventById(1L)).thenReturn(event);

        mockMvc.perform(
            get("/api/scheduling/event/1")
                .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(handler().methodName("getEvent"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(event.getId()));
    }

    @Test
    @WithMockUser
    void getEventBySchedule() throws Exception {
        when(scheduleService.getScheduleById(1L)).thenReturn(schedule);
        when(eventService.getEventBySchedule(schedule)).thenReturn(event);

        mockMvc.perform(
            get("/api/scheduling/event/geteventbyschedule/1")
                .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(handler().methodName("getEventBySchedule"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(event.getId()));
    }

    @Test
    @WithMockUser
    void getEventsOfUser() throws Exception {
        when(eventService.getEventsOfUser(user)).thenReturn(Arrays.asList(event));

        String token = "Bearer " + JwtUtil.generateToken(userDetails);

        mockMvc.perform(
            get("/api/scheduling/event/getusersevents")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", token)
            )
            .andExpect(handler().methodName("getEventsOfUser"))
            .andExpect(status().isOk());
    }
     

}

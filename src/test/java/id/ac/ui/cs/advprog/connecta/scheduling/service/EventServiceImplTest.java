package id.ac.ui.cs.advprog.connecta.scheduling.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.notification.service.NotificationService;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.EventRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EventServiceImplTest {
    
    @InjectMocks
    private EventService eventService = new EventServiceImpl();

    @Mock
    private EventRepository eventRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private NotificationService notificationService;

    private User user;
    private Subject subject;
    private Event event;
    private Schedule schedule;

    @BeforeEach
    public void setUp() {
        user = new User("Testy McTesty", "test@test.com", "test123");
        subject = new Subject("SubjectTitle", "SubjectDesc");
        event = new Event("EventTitle", subject, user, LocalDate.now(), LocalDate.now(),"https://meet.google.com/gud-ayae-ced?authuser=0&pli=1");
        schedule = new Schedule(event, event.getStartDate(), LocalTime.now().plusMinutes(10), LocalTime.now().plusMinutes(20));
    }

    @Test
    void createEvent() {
        Event resultEvent = eventService.createEvent(event);
        assertEquals(event, resultEvent);
    }

    @Test
    void getEventById() {
        when(eventRepository.findEventById(1L)).thenReturn(event);

        Event resultEvent = eventService.getEventById(1L);
        assertEquals(event, resultEvent);
    }

    @Test
    void getEventBySchedule() {
        when(eventRepository.findEventBySchedule(schedule)).thenReturn(event);

        Event resultEvent = eventService.getEventBySchedule(schedule);
        assertEquals(event, resultEvent);
    }

    @Test
    void getAllEvent() {
        when(eventRepository.findAll()).thenReturn(Arrays.asList(event));

        List<Event> allEvent = eventService.getAllEvent();
        
        boolean eventExists = false;

        for (Event eventInList: allEvent) {
            if (eventInList.equals(event)) {
                eventExists = true;
            }
        }

        assertTrue(eventExists);
    }

    @Test
    void getEventsOfUser() {
        user.setCreatedEvents(Arrays.asList(event));
        when(userRepository.findUserById(user.getId())).thenReturn(Optional.of(user));
        // when(eventService.getEventsOfUser(user)).thenReturn(Arrays.asList(event));
        
        List<Event> eventsOfUser = eventService.getEventsOfUser(user);
        
        boolean eventExists = false;

        for (Event eventInList: eventsOfUser) {
            if (eventInList.equals(event)) {
                eventExists = true;
            }
        }

        assertTrue(eventExists);
    } 
}

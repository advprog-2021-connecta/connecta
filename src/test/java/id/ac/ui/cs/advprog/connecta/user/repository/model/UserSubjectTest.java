package id.ac.ui.cs.advprog.connecta.user.repository.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

class UserSubjectTest {
    @Test
    void createWithNoArgsConstructor() {
        UserSubject us = new UserSubject();

        assertNotNull(us);
    }
}

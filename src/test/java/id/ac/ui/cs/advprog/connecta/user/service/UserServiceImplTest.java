package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @Mock
    private UserRepository userRepository;

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
    }

    @Test
    void createUser() {
        User resultUser = userService.createUser(user);
        assertEquals(user, resultUser);
    }

    @Test
    void getListUser() {
        when(userRepository.findAll()).thenReturn(Arrays.asList(user));

        Iterable<User> userList = userService.getListUser();
        boolean userIsInUserList = false;
        for (User userInList : userList) {
            if (userInList.equals(user)) {
                userIsInUserList = true;
            }
        }

        assertTrue(userIsInUserList);
    }

    @Test
    void getUserById() {
        when(userRepository.findUserById(1L)).thenReturn(Optional.of(user));

        User resultUser = userService.getUserById(1L);
        assertEquals(user, resultUser);
    }

    @Test
    void getUserByEmail() {
        when(userRepository.findUserByEmail(user.getEmail())).thenReturn(Optional.of(user));

        User resultUser = userService.getUserByEmail(user.getEmail());
        assertEquals(user, resultUser);
    }

    @Test
    void updateUser() {
        String newFullName = "New Name";
        user.setFullName(newFullName);
        User resultUser = userService.updateUser(3L, user);

        assertEquals(3L, resultUser.getId());
        assertEquals(newFullName, resultUser.getFullName());
    }

    @Test
    void deleteUserById() {
        userService.deleteUserById(1L);
        verify(userRepository, atLeastOnce()).deleteById(1L);
    }
}
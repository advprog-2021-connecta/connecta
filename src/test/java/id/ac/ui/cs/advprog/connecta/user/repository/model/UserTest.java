package id.ac.ui.cs.advprog.connecta.user.repository.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void setPassword() {
        User user = new User("dummy", "dummy@dummy.com", "dummy");
        String newPassword = "newPassword";

        user.setPassword(newPassword);
        assertNotEquals(newPassword, user.getPassword());
    }
}
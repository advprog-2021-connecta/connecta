package id.ac.ui.cs.advprog.connecta.frontend.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = EventPageController.class)
class EventPageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;

    @Test
    void whenIndexUrlAccessedShouldReturnIndexPage() throws Exception {
        mockMvc.perform(get("/scheduling/index"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/scheduling/index"));
    }

    @Test
    void whenCreateEventUrlAccessedShouldReturnCreateEventPage() throws Exception {
        mockMvc.perform(get("/scheduling/createevent/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/scheduling/createevent"));
    }
    
    @Test
    void whenEventDetailsUrlAccessedShouldReturnEventDetailsPage() throws Exception {
        mockMvc.perform(get("/scheduling/eventdetails/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/scheduling/eventdetails"));
    }

    @Test
    void whenCreateScheduleUrlAccessedShouldReturnCreateSchedulePage() throws Exception {
        mockMvc.perform(get("/scheduling/createschedule/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/scheduling/createschedule"));
    }

    @Test
    void canOpenBookSchedulePage() throws Exception {
        mockMvc.perform(get("/scheduling/1/book"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/scheduling/bookschedule"));
    }
}

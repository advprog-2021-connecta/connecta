package id.ac.ui.cs.advprog.connecta.booking.service;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.connecta.booking.repository.SubjectRepository;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.UserSubjectRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;

@ExtendWith(MockitoExtension.class)
class SubjectServiceImplTest {
    @InjectMocks
    private SubjectService subjectSvc = new SubjectServiceImpl();

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserSubjectRepository userSubjectRepository;
    
    @Mock
    private SubjectRepository subjectRepository;

    private User user;

    private Subject subject;

    @BeforeEach
    public void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
        subject = new Subject("AP", "Advanced Programming");
    }

    @Test
    void testFindAllEligibleSubjects_OK() {
        List<UserSubject> userSubjects = new ArrayList<>();
        List<Subject> subjects = new ArrayList<>();
        subjects.add(subject); 

        when(userRepository.findUserByEmail(user.getEmail())).thenReturn(Optional.of(user));
        when(userSubjectRepository.findByUser(user)).thenReturn(userSubjects);
        when(subjectRepository.findAll()).thenReturn(subjects);

        List<Subject> res = subjectSvc.findAllEligibleSubjects(user.getEmail());
        assertEquals(subjects.size(), res.size());
    }

    @Test
    void testFindAllEligibleSubjects_UserNotFound() {
        List<UserSubject> userSubjects = new ArrayList<>();
        List<Subject> subjects = new ArrayList<>();
        subjects.add(subject); 

        when(userRepository.findUserByEmail(user.getEmail())).thenReturn(null);

        assertThrows(NullPointerException.class, () -> {
            List<Subject> res = subjectSvc.findAllEligibleSubjects(user.getEmail());
        });
    }

    @Test
    void testEnrollToSubject_OK() {
        UserSubject enrollmentObj = new UserSubject();
        enrollmentObj.setSubject(subject);
        enrollmentObj.setUser(user);
        enrollmentObj.setIsTeachingAssistant(false);

        SubjectService mockSubjectSvc = mock(SubjectService.class);

        mockSubjectSvc.enrollToSubject(subject.getId(), user.getEmail());
        verify(mockSubjectSvc, times(1)).enrollToSubject(subject.getId(), user.getEmail());
    }

    @Test
    void testUnenrollSubject_OK() {
        UserSubject enrollmentObj = new UserSubject();
        enrollmentObj.setSubject(subject);
        enrollmentObj.setUser(user);
        enrollmentObj.setIsTeachingAssistant(false);
        enrollmentObj.setId(1L);

        SubjectService mockSubjectSvc = mock(SubjectService.class);

        mockSubjectSvc.unenrollFromSubject(enrollmentObj.getId());
        verify(mockSubjectSvc, times(1)).unenrollFromSubject(enrollmentObj.getId());
    }

    @Test
    void testCreateSubject_OK() {
        when(subjectRepository.save(subject)).thenReturn(subject);

        Subject res = subjectSvc.createSubject(subject);
        assertEquals(subject.getTitle(), res.getTitle());
        assertEquals(subject.getDescription(), res.getDescription());
    }

    @Test
    void testGetSubjectById_OK() {
        when(subjectRepository.findSubjectById(subject.getId())).thenReturn(Optional.of(subject));

        Subject res = subjectSvc.getSubjectById(subject.getId());
        assertEquals(subject.getTitle(), res.getTitle());
        assertEquals(subject.getDescription(), res.getDescription());
    }

    @Test
    void testGetSubjectById_NotFound() {
        when(subjectRepository.findSubjectById(subject.getId())).thenReturn(null);

        assertThrows(NullPointerException.class,() -> { subjectSvc.getSubjectById(subject.getId()); });
    }


}

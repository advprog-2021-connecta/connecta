package id.ac.ui.cs.advprog.connecta.notification;

import id.ac.ui.cs.advprog.connecta.notification.core.MessagesBooking;
import id.ac.ui.cs.advprog.connecta.notification.core.MessagesEvent;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

@ExtendWith(MockitoExtension.class)
class MessagesTest {

    private Schedule schedule;
    private Event event;
    private User asdos;
    private Subject subject;
    private LocalTime time1;
    private LocalTime time2;


    @BeforeEach
    public void before() {
        time1 = LocalTime.of(11,15);
        time2 = LocalTime.of(12,15);

        asdos = new User("asdos", "example@example.com", "test123");
        subject = new Subject("SubjectTitle", "SubjectDesc");
        event = new Event("EventTest", subject, asdos, LocalDate.now(), LocalDate.now(),"https://meet.google.com/gud-ayae-ced?authuser=0&pli=1");
        event.setId(1L);
        schedule = new Schedule(event, event.getStartDate(), time1, time2);
    }

    @Test
    void getMethodEventTest() {
        MessagesEvent messagesEvent = new MessagesEvent(asdos.getEmail(), event);
        assertEquals(messagesEvent.getEmail(),"example@example.com");
        assertEquals(messagesEvent.getCreator(),"asdos");
        assertEquals(messagesEvent.getTitle(),"EventTest");
        assertEquals(messagesEvent.getSubject(),"SubjectTitle");
        assertEquals(messagesEvent.getEndDate(),LocalDate.now().toString());
        assertEquals(messagesEvent.getStartDate(),LocalDate.now().toString());
    }

    @Test
    void classHasDefaultConstructorTest(){
        assertNotNull(new MessagesEvent());
        assertNotNull(new MessagesBooking());
    }

    @Test
    void getMethodBookingTest() {
        schedule.setParticipant(new User("mahasiswa", "example@example.com", "test123"));
        MessagesBooking messagesBooking = new MessagesBooking(asdos.getEmail(),schedule);
        assertEquals(messagesBooking.getEmail(),"example@example.com");
        assertEquals(messagesBooking.getCreator(),"asdos");
        assertEquals(messagesBooking.getEvent(),"EventTest");
        assertEquals(messagesBooking.getParticipant(),"mahasiswa");
        assertEquals(messagesBooking.getDate(), LocalDate.now().toString());
        assertEquals(messagesBooking.getEndTime(),time2.toString());
        assertEquals(messagesBooking.getStartTime(),time1.toString());
    }
    }


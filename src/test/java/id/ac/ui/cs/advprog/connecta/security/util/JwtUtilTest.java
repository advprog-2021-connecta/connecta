package id.ac.ui.cs.advprog.connecta.security.util;

import id.ac.ui.cs.advprog.connecta.user.repository.model.ConnectaUserDetails;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class JwtUtilTest {

    private User user;
    private UserDetails userDetails;

    @BeforeEach
    public void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
        userDetails = new ConnectaUserDetails(user.getEmail());
    }

    @Test
    void validateToken() {
        String generatedToken = JwtUtil.generateToken(userDetails);
        assertTrue(JwtUtil.validateToken(generatedToken, userDetails));
    }

    @Test
    void getEmailFromToken() {
        String generatedToken = JwtUtil.generateToken(userDetails);
        String extractedEmail = JwtUtil.extractEmail(generatedToken);

        assertEquals(user.getEmail(), extractedEmail);
    }

    @Test
    void getExpirationDateFromToken() {
        String generatedToken = JwtUtil.generateToken(userDetails);
        Date extractedExpirationDate = JwtUtil.extractExpiration(generatedToken);

        assertTrue(extractedExpirationDate.after(new Date()));
    }
}
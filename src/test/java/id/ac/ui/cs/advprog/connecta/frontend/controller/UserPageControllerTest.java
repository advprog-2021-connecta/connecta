package id.ac.ui.cs.advprog.connecta.frontend.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = AdminPageController.class)
class UserPageControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;

    @Test
    void canOpenDashboardPage() throws Exception {
        mockMvc.perform(get("/admin/index"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/dashboard"));
    }

    @Test
    void canOpenUsersPage() throws Exception {
        mockMvc.perform(get("/admin/users"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/users/index"));
    }

    @Test
    void canOpenUserDetailPage() throws Exception {
        mockMvc.perform(get("/admin/users/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/users/view"));
    }

    @Test
    void canOpenSubjectsPage() throws Exception {
        mockMvc.perform(get("/admin/subjects"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/subjects/index"));
    }
}

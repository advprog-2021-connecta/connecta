package id.ac.ui.cs.advprog.connecta.scheduling.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import id.ac.ui.cs.advprog.connecta.Util;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.EventRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.ScheduleRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.scheduling.service.EventService;
import id.ac.ui.cs.advprog.connecta.scheduling.service.ScheduleService;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;

@WebMvcTest(controllers = ScheduleController.class)
class ScheduleControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScheduleService scheduleService;

    @MockBean
    private EventService eventService;

    @MockBean
    private UserService userService;

    @MockBean
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    private User user;
    private Subject subject;
    private Event event;
    private Schedule schedule;

    @BeforeEach
    public void setUp() {
        user = new User("Testy McTesty", "test@test.com", "test123");
        subject = new Subject("SubjectTitle", "SubjectDesc");
        event = new Event("EventTitle", subject, user, LocalDate.now(), LocalDate.now(),"https://meet.google.com/gud-ayae-ced?authuser=0&pli=1");
        schedule = new Schedule(event, LocalDate.now(), LocalTime.now().plusMinutes(10), LocalTime.now().plusMinutes(20));
        schedule.setId(1L);
    }

    @Test
    @WithMockUser
    void createScheduleSuccess() throws Exception {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        when(eventService.getEventById(1L)).thenReturn(event);
        when(scheduleService.getSchedulesOfEvent(1L)).thenReturn(new ArrayList<Schedule>());

        Object bodyContent = new Object() {
            public final Long eventId = 1L;
            public final String date = schedule.getDate().format(dateFormatter);
            public final String startTime = schedule.getStartTime().format(timeFormatter);
            public final String endTime = schedule.getEndTime().format(timeFormatter);
        };

        mockMvc.perform(
            post("/api/scheduling/schedule")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Util.mapToJson(bodyContent))
        )
        .andExpect(handler().methodName("createSchedule"))
        .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void getSchedulesOfEvent() throws Exception {
        when(scheduleService.getSchedulesOfEvent(1L)).thenReturn(Arrays.asList(schedule));

        mockMvc.perform(
            get("/api/scheduling/schedule/1")
                .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(handler().methodName("getSchedulesOfEvent"))
        .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void getAllSchedule() throws Exception {
        when(scheduleService.getAllSchedule()).thenReturn(Arrays.asList(schedule));

        mockMvc.perform(
            get("/api/scheduling/schedule/getAllSchedule")
                .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(handler().methodName("getAllSchedule"))
        .andExpect(status().isOk());
    }
}

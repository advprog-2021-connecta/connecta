package id.ac.ui.cs.advprog.connecta.security.filter;

import id.ac.ui.cs.advprog.connecta.security.util.JwtUtil;
import id.ac.ui.cs.advprog.connecta.user.controller.UserController;
import id.ac.ui.cs.advprog.connecta.user.repository.model.ConnectaUserDetails;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;
import id.ac.ui.cs.advprog.connecta.user.service.UserSubjectService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = UserController.class)
class JwtRequestFilterTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;

    @MockBean
    UserSubjectService userSubjectService;

    @MockBean
    private UserService userService;

    @Test
    void testAccessAdminRequiredEndpoint() throws Exception {
        User admin = new User("admin", "admin@admin.com", "admin");
        admin.setIsAdmin(true);
        UserDetails adminUserDetails = new ConnectaUserDetails(admin);
        String jwtToken = JwtUtil.generateToken(adminUserDetails);
        List<User> userList = Arrays.asList(admin);

        when(userDetailsService.loadUserByUsername("admin@admin.com")).thenReturn(adminUserDetails);
        when(userService.getListUser()).thenReturn(userList);

        mockMvc.perform(
                get("/api/user")
                        .header("Authorization", "Bearer " + jwtToken)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getListUser"))
                .andExpect(jsonPath("$[0].email").value(admin.getEmail()))
                .andDo(MockMvcResultHandlers.log());
        ;
    }
}
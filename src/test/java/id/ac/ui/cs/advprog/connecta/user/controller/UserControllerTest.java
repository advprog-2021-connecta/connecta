package id.ac.ui.cs.advprog.connecta.user.controller;

import id.ac.ui.cs.advprog.connecta.Util;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;
import id.ac.ui.cs.advprog.connecta.user.service.UserSubjectService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserSubjectService userSubjectSvc;

    @MockBean
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    private User user;
    private String userRawPassword;

    @BeforeEach
    public void setUp() {
        userRawPassword = "dummy123";
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", userRawPassword);
    }

    @Test
    void createUser() throws Exception {
        Object bodyContent = new Object() {
            public final String fullName = user.getFullName();
            public final String email = user.getEmail();
            public final String password = userRawPassword;
        };

        when(userService.createUser(user)).thenReturn(user);

        mockMvc.perform(
                post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapToJson(bodyContent))
                )
                .andExpect(handler().methodName("createUser"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getListUser() throws Exception {

        List<User> userList = Arrays.asList(user);
        when(userService.getListUser()).thenReturn(userList);

        mockMvc.perform(
                        get("/api/user")
                            .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getListUser"))
                .andExpect(jsonPath("$[0].email").value(user.getEmail()))
        ;
    }

    @Test
    @WithMockUser
    void getUser() throws Exception {
        when(userService.getUserById(1L)).thenReturn(user);

        mockMvc.perform(
                        get("/api/user/1")
                            .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getUser"))
                .andExpect(jsonPath("$.email").value(user.getEmail()))
        ;
    }

    @Test
    @WithMockUser
    void getUserNotFound() throws Exception {

        mockMvc.perform(
                        get("/api/user/2")
                            .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound())
                .andExpect(handler().methodName("getUser"))
        ;
    }

    @Test
    @WithMockUser
    void updateUser() throws Exception {
        String newFullName = "Brand New Name";
        User newUser = new User(newFullName, user.getEmail(), userRawPassword);

        Object bodyContent = new Object() {
            public final String fullName = newUser.getFullName();
            public final String email = newUser.getEmail();
            public final String password = newUser.getPassword();
        };

        when(userService.updateUser(anyLong(), any(User.class))).thenReturn(newUser);

        mockMvc.perform(
                        put("/api/user/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Util.mapToJson(bodyContent))
                )
                .andExpect(status().isOk())
                .andExpect(handler().methodName("updateUser"))
                .andExpect(jsonPath("$.fullName").value(newFullName))
        ;
    }

    @Test
    @WithMockUser
    void deleteUser() throws Exception {
        mockMvc.perform(delete("/api/user/1"))
                .andExpect(handler().methodName("deleteUser"))
                .andExpect(status().isNoContent())
        ;
    }
    
    @Test
    @WithMockUser
    void updateUserSubject_OK() throws Exception {
        UserSubject req = new UserSubject();
        req.setId(1L);
        when(userSubjectSvc.updateUserSubject(any(UserSubject.class))).thenReturn(req);
        mockMvc.perform(put("/api/user/1/userSubject").contentType(MediaType.APPLICATION_JSON).content("{\"isTeachingAssistant\": true}".getBytes()))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("updateUserSubject"));
    }

    @Test
    @WithMockUser
    void updateUserSubject_UserNotFound() throws Exception {
        UserSubject req = new UserSubject();
        req.setId(1L);
        when(userSubjectSvc.updateUserSubject(any(UserSubject.class))).thenReturn(null);
        mockMvc.perform(put("/api/user/1/userSubject").contentType(MediaType.APPLICATION_JSON).content("{\"isTeachingAssistant\": true}".getBytes()))
            .andExpect(status().isNotFound())
            .andExpect(handler().methodName("updateUserSubject"));
    }

    @Test
    @WithMockUser
    void updateUserSubjectError() throws Exception {
        UserSubject req = new UserSubject();
        req.setId(1L);
        when(userSubjectSvc.updateUserSubject(any(UserSubject.class))).thenThrow(NullPointerException.class);
        mockMvc.perform(put("/api/user/1/userSubject").contentType(MediaType.APPLICATION_JSON).content("{\"isTeachingAssistant\": true}".getBytes()))
            .andExpect(handler().methodName("updateUserSubject"))
            .andExpect(status().is5xxServerError());
    }

    @Test
    @WithMockUser
    void findAllUserSubject() throws Exception {
        Subject mockSubject = new Subject("AP", "Advance Programming");
        UserSubject us = new UserSubject(1L, false, user, mockSubject);
        ArrayList<UserSubject> res = new ArrayList<>();
        res.add(us);
        when(userSubjectSvc.findAllUserSubjectByUserId(1L)).thenReturn(res);
        
        mockMvc.perform(get("/api/user/1/userSubject"))
        .andExpect(status().isOk())
        .andExpect(handler().methodName("findAllUserSubject"));
    }
}
package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConnectaUserDetailsServiceTest {

    @Mock
    UserService userService;

    @InjectMocks
    ConnectaUserDetailsService userDetailsService;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User("Dummy McDumbDumb", "dummy@dummy.com", "dummy123");
    }

    @Test
    void loadUserByUsername() {
        when(userService.getUserByEmail(user.getEmail())).thenReturn(user);

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        assertEquals(user.getEmail(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
    }
}
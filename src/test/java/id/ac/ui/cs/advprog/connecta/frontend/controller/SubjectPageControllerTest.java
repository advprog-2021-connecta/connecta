package id.ac.ui.cs.advprog.connecta.frontend.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = SubjectPageController.class)
class SubjectPageControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;

    @Test
    void whenIndexUrlAccessedShouldReturnIndexPage() throws Exception {
        mockMvc.perform(get("/subjects/createsubject"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/subjects/createsubject"));
    }
}

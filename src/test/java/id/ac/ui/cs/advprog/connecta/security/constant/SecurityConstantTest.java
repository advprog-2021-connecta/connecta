package id.ac.ui.cs.advprog.connecta.security.constant;

import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SecurityConstantTest {

    @Test
    void testSignatureAlgorithm() {
        assertEquals(SignatureAlgorithm.HS256, SecurityConstant.SIGNATURE_ALGORITHM);
    }
}
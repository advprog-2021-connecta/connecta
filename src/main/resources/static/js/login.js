$(document).ready(function () {

    if (isLoggedIn()) {
        window.location.replace("/admin/index");
    }

    $("#loginForm").submit(function (e) {

        e.preventDefault(); // prevent actual submit of the form.

        let form = $(this);
        let url = form.attr('action');

        const body = {
            email: $("#email").val(),
            password: $("#password").val(),
        }

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(body),
            success: function (data) {
                setCookie("token", data.jwt, 10)

                window.location.replace("/admin/index");
            },
            error: function (error) {
                console.error(error);
                alert("Wrong email or password");
            }
        });
    });
});

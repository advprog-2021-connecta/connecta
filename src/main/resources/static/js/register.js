$(document).ready(function () {

    if (isLoggedIn()) {
        window.location.replace("/admin/index");
    }

    $("#registerForm").submit(function (e) {

        e.preventDefault(); // prevent actual submit of the form.

        let form = $(this);
        let url = form.attr('action');

        if ($("#password").val() !== $("#passwordConfirm").val()) {
            alert("Wrong password confirmation")
            return;
        }

        const body = {
            email: $("#email").val(),
            fullName: $("#fullName").val(),
            password: $("#password").val(),
        }

        console.log(body)

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(body),
            success: function (data) {
                alert("You are successfully registered. Now login!");
                window.location.replace("/auth/login");
            },
            error: function (error) {
                console.error(error);
                alert("Email already registered. Please register with different email");
            }
        });
    });
});

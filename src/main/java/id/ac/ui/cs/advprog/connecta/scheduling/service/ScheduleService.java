package id.ac.ui.cs.advprog.connecta.scheduling.service;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;

import java.util.List;

public interface ScheduleService {
    public Schedule createSchedule(Schedule schedule);
    public void enroll(String reqUserEmail, Long scheduleId);
    public List<Schedule> getSchedulesOfEvent(Long id);
    public List<Schedule> getAllSchedule();
    public Schedule getScheduleById(Long id);
}
package id.ac.ui.cs.advprog.connecta.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthenticationFrontendController {
    @RequestMapping("/login")
    public String userLogin() {
        return "auth/login";
    }

    @RequestMapping("/register")
    public String userRegister() {
        return "auth/register";
    }
}

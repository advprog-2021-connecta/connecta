package id.ac.ui.cs.advprog.connecta.scheduling.service;

import id.ac.ui.cs.advprog.connecta.booking.model.Booking;
import id.ac.ui.cs.advprog.connecta.notification.service.NotificationService;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.EventRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.ScheduleRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    NotificationService notificationService;

    @Override
    public Schedule createSchedule(Schedule schedule) {
        scheduleRepository.save(schedule);

        return schedule;
    }

    @Override
    public void enroll(String reqUserEmail, Long scheduleId) {
        Optional<User> student = userRepository.findUserByEmail(reqUserEmail);
        Schedule schedule = scheduleRepository.findScheduleById(scheduleId);

        if (student.isPresent()) {
            schedule.setParticipant(student.get());
            schedule.setAvailable(false);

            userRepository.save(student.get());
            scheduleRepository.save(schedule);
        }

        if (student.isPresent() && schedule != null) {
            Booking booking = new Booking();
            booking.setBookingDate(new Date());
            booking.setStatus(1);
            booking.setStudent(student.get());
            booking.setTeachingAssistant(schedule.getEvent().getCreator());
            booking.setJadwal(schedule);
            
            notificationService.notifyNewBooking(booking);
        }
        
    }

    @Override
    public List<Schedule> getSchedulesOfEvent(Long id) {
        Event event = eventRepository.findEventById(id);
        
        return event.getSchedule();
    }

    @Override
    public List<Schedule> getAllSchedule() {
        return scheduleRepository.findAll();
    }

    @Override
    public Schedule getScheduleById(Long id) {
        return scheduleRepository.findScheduleById(id);
    }
}

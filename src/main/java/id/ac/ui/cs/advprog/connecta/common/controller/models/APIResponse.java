package id.ac.ui.cs.advprog.connecta.common.controller.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class APIResponse {
    @JsonInclude(Include.NON_NULL)
    private Map<?, ?> data;

    @JsonInclude(Include.NON_NULL)
    private ErrorResponse error;
}
package id.ac.ui.cs.advprog.connecta.scheduling.service;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;

import java.util.List;


public interface EventService {
    public Event createEvent(Event event);
    public Event getEventById(Long id);
    public Event getEventBySchedule(Schedule schedule);
    public List<Event> getAllEvent();
    public List<Event> getEventsOfUser(User req);
}

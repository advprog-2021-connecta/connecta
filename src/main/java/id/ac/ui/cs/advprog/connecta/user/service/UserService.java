package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.user.repository.model.User;

public interface UserService {

    User createUser(User user);

    Iterable<User> getListUser();

    User getUserById(Long id);

    User getUserByEmail(String email);

    User updateUser(Long id, User user);

    void deleteUserById(Long id);

}

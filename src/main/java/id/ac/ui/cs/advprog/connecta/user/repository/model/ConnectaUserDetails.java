package id.ac.ui.cs.advprog.connecta.user.repository.model;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@NoArgsConstructor
public class ConnectaUserDetails implements UserDetails {

    private String username;
    private String password;
    private Boolean isAdmin;

    public ConnectaUserDetails(User user) {
        this.username = user.getEmail();
        this.password = user.getPassword();
        this.isAdmin = user.getIsAdmin();
    }

    public ConnectaUserDetails(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.getIsAdmin()
                ? Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))
                : Collections.EMPTY_LIST;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Boolean getIsAdmin() {
        return this.isAdmin;
    }
}

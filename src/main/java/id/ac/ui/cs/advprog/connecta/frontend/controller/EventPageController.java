package id.ac.ui.cs.advprog.connecta.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/scheduling")
public class EventPageController {
    
    @RequestMapping("/index")
    public String index() {
        return "admin/scheduling/index";
    }

    @RequestMapping("/createevent/{id}")
    public String createEvent(@PathVariable(value = "id") Long id) {
        return "admin/scheduling/createevent";
    }

    @RequestMapping("/eventdetails/{id}")
    public String eventDetails(@PathVariable(value = "id") Long id) {
        return "admin/scheduling/eventdetails";
    }

    @RequestMapping("/createschedule/{id}")
    public String createSchedule(@PathVariable(value = "id") Long id) {
        return "admin/scheduling/createschedule";
    }

    @RequestMapping("/{id}/book")
    public String bookEvent(@PathVariable(value = "id") Long id) {
        return "admin/scheduling/bookschedule";
    }
}

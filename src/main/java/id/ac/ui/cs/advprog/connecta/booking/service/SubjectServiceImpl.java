package id.ac.ui.cs.advprog.connecta.booking.service;

import id.ac.ui.cs.advprog.connecta.booking.repository.SubjectRepository;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.UserSubjectRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectRepository subjectRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserSubjectRepository userSubjectRepo;

    public List<Subject> findAllEligibleSubjects(String email) {
        Optional<User> user = userRepo.findUserByEmail(email);
        if (user.isPresent()) {
            List<UserSubject> enrolledSubjects = userSubjectRepo.findByUser(user.get());

            List<Subject> res = subjectRepo.findAll();

            for (UserSubject subject : enrolledSubjects) {
                res.remove(subject.getSubject());
            }

            for (UserSubject subject : enrolledSubjects) {
                subject.getSubject().setHasEnrolled(true);
                subject.getSubject().setEnrollmentId(subject.getId());
                subject.getSubject().setTeachingAssistant(subject.getIsTeachingAssistant());
                res.add(subject.getSubject());
            }

            return res;
        }

        return null;
    }
    
    public void enrollToSubject(Long subjectId, String email) {
        Optional<User> user = userRepo.findUserByEmail(email);
        Optional<Subject> subject = subjectRepo.findById(subjectId);
        var usrSubject = new UserSubject();
        if (subject.isPresent()) {
            usrSubject.setSubject(subject.get());

            if (user.isPresent()) {
                usrSubject.setUser(user.get());
                usrSubject.setIsTeachingAssistant(false);

                userSubjectRepo.save(usrSubject);
            }
        }
        
    }

    public void unenrollFromSubject(Long subjectId) {
        userSubjectRepo.deleteById(subjectId);
    }

    public Subject createSubject(Subject subject) {
        subjectRepo.save(subject);
        return subject;
    }

    public Subject getSubjectById(Long id) {
        Optional<Subject> subject = subjectRepo.findSubjectById(id);

        if (subject.isPresent()) {
            return subject.get();
        }

        return null;
    }
}

package id.ac.ui.cs.advprog.connecta.notification.core;


import id.ac.ui.cs.advprog.connecta.notification.config.RabbitMqConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;


@Component
public class Email {

    @Autowired
    private JavaMailSender sender;

    private String address;
    private String header;
    private String content;

    @RabbitListener(queues= RabbitMqConfig.QUEUE_NAME_1, containerFactory="container")
    public void onMessage(MessagesEvent message){
        try {
            setHeader("New Event From Subject "+ message.getSubject());
            setContent("Event "+message.getTitle()+ " created by "+message.getCreator()+" .You can book the schedule from "+message.getStartDate()+" until "+message.getEndDate()+" via this link http://188.166.182.181/scheduling/"+message.getEventId() +"/book . Book the schedule now!");
            setAddress(message.getEmail());
            sendEmail();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RabbitListener(queues= RabbitMqConfig.QUEUE_NAME_2, containerFactory="container")
    public void onMessage(MessagesBooking message){
        try {
            setHeader("A Schedule From "+message.getEvent()+" is Booked");
            setContent("Time: "+message.getStartTime()+"-"+message.getEndTime()+
                    "\nDate: "+message.getDate()+
                    "\nTeaching Assistant: "+message.getCreator()+
                    "\nParticipant: "+message.getParticipant()+
                    "\nLink Meeting: "+ message.getLink()
            );
            setAddress(message.getEmail());
            sendEmail();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendEmail(){
        try {
            var message = sender.createMimeMessage();
            var helper = new MimeMessageHelper(message);
            helper.setTo(address);
            helper.setText(content);
            helper.setSubject(header);
            sender.send(message);
        }
        catch (Exception ex){
            System.out.println("Email not send"+ex);
        }
    }

    public void setAddress(String address){
        this.address = address;
    }
    public void setContent(String content){ this.content = content; }
    public void setHeader(String header){
        this.header = header;
    }




}

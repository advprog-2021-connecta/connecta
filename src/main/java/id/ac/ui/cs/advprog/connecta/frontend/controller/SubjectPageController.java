package id.ac.ui.cs.advprog.connecta.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/subjects")
public class SubjectPageController {
    
    @RequestMapping("/createsubject")
    public String createSubject() {
        return "admin/subjects/createsubject";
    }
}
package id.ac.ui.cs.advprog.connecta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConnectaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConnectaApplication.class, args);
	}

}

package id.ac.ui.cs.advprog.connecta.scheduling.repository;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {
    public Event findEventById(Long Id);
    public Event findEventBySchedule(Schedule schedule);
}

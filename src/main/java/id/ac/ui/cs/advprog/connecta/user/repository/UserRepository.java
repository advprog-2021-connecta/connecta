package id.ac.ui.cs.advprog.connecta.user.repository;

import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public Optional<User> findUserById(Long id);

    public Optional<User> findUserByEmail(String email);
}

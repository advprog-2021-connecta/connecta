package id.ac.ui.cs.advprog.connecta.notification.core;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import lombok.Data;

@Data
public class MessagesEvent {

    private String email;
    private String creator;
    private String subject;
    private String title;
    private String startDate;
    private String endDate;
    private String eventId;

    public MessagesEvent() {
        
    }

    public MessagesEvent(String email, Event event){
        this.email = email;
        this.creator = event.getCreator().getFullName();
        this.subject = event.getSubject().getTitle();
        this.title =  event.getTitle();
        this.startDate = event.getStartDate().toString();
        this.endDate = event.getStartDate().toString();
        this.eventId = event.getId().toString();
    }


}

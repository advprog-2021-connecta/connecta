package id.ac.ui.cs.advprog.connecta.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/admin")
public class AdminPageController {

    @RequestMapping("/index")
    public String index() {
        return "admin/dashboard";
    }

    @RequestMapping("/users")
    public String usersIndex() {
        return "admin/users/index";
    }

    @RequestMapping("/users/{id}")
    public String usersDetail(@PathVariable(value = "id") Long id) {
        return "admin/users/view";
    }

    @RequestMapping("/subjects")
    public String subjectsIndex() {
        return "admin/subjects/index";
    }
}

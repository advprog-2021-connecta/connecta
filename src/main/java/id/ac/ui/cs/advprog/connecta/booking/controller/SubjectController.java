package id.ac.ui.cs.advprog.connecta.booking.controller;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.booking.service.SubjectService;
import id.ac.ui.cs.advprog.connecta.security.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/subjects")
public class SubjectController {
    @Autowired
    private SubjectService subjectSvc;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Subject>> findAllSubjects(@RequestHeader(name="Authorization") String token) {
        String userEmail = JwtUtil.extractEmail(token.split("Bearer ", -1)[1]);
        return ResponseEntity.ok(subjectSvc.findAllEligibleSubjects(userEmail));
    }

    @PutMapping(produces = {"application/json"}, path = "/{id}/enroll")
    @ResponseBody
    public ResponseEntity<?> enrollToSubject(@RequestHeader(name="Authorization") String token, @PathVariable(value = "id") Long id) {
        String userEmail = JwtUtil.extractEmail(token.split("Bearer ", -1)[1]);
        subjectSvc.enrollToSubject(id, userEmail);
        Map<String, String> res = new HashMap<>();
        res.put("message", "ok");
        return ResponseEntity.ok(res);
    }

    @PutMapping(produces = {"application/json"}, path = "/{id}/unenroll")
    @ResponseBody
    public ResponseEntity<?> unenrollFromSubject(@RequestHeader(name="Authorization") String token, @PathVariable(value = "id") Long id) {
        subjectSvc.unenrollFromSubject(id);

        Map<String, String> res = new HashMap<>();
        res.put("message", "ok");
        return ResponseEntity.ok(res);
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> createSubject(@RequestBody Map<String, String> requestBody) {       
        Subject subject = new Subject(
            requestBody.get("title"),
            requestBody.get("description")
        );

        return ResponseEntity.ok(subjectSvc.createSubject(subject));
    }
}

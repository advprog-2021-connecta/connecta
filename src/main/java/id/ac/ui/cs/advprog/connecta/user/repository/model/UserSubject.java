package id.ac.ui.cs.advprog.connecta.user.repository.model;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_subjects")
public class UserSubject {
    private @Id @GeneratedValue Long id;
    private Boolean isTeachingAssistant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    private Subject subject;
    
}

package id.ac.ui.cs.advprog.connecta.scheduling.service;

import id.ac.ui.cs.advprog.connecta.notification.service.NotificationService;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.EventRepository;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private UserRepository userRepository;

//    @Autowired
//    private ScheduleRepository scheduleRepository;

    @Override
    public Event createEvent(Event event) {
        eventRepository.save(event);
        notificationService.notifyNewEvent(event);
        return event;
    }

    @Override
    public Event getEventById(Long id) {
        return eventRepository.findEventById(id);
    }

    @Override
    public Event getEventBySchedule(Schedule schedule) {
        return eventRepository.findEventBySchedule(schedule);
    }

    @Override
    public List<Event> getAllEvent() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> getEventsOfUser(User req) {
        Optional<User> user = userRepository.findUserById(req.getId());

        if (user.isPresent()) {
            return user.get().getCreatedEvents();
        }

        return null;
    }

}

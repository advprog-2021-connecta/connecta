package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public Iterable<User> getListUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        Optional user = userRepository.findUserById(id);

        return (User) user.orElse(null);
    }

    @Override
    public User getUserByEmail(String email) {
        Optional user = userRepository.findUserByEmail(email);

        return (User) user.orElse(null);
    }

    @Override
    public User updateUser(Long id, User user) {
        user.setId(id);
        userRepository.save(user);
        return user;
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }
}

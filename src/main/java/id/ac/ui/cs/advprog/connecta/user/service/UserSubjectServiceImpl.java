package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.booking.repository.SubjectRepository;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.user.repository.UserRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.UserSubjectRepository;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSubjectServiceImpl implements UserSubjectService {

    @Autowired
    private UserSubjectRepository userSubjectRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private SubjectRepository subjectRepo;


    public UserSubject updateUserSubject(UserSubject req) {
        Optional<UserSubject> userSubject = userSubjectRepo.findById(req.getId());
        if (userSubject.isPresent()) {
            userSubject.get().setIsTeachingAssistant(req.getIsTeachingAssistant());
            userSubjectRepo.save(userSubject.get());
            return userSubject.get();
        } else {
            return null;
        }
    }
    
    public Iterable<UserSubject> findAllUserSubjectByUserId(Long userId) {
        Optional<User> user = userRepo.findById(userId);
        if(user.isEmpty()) {
            return null;
        }

        Iterable<UserSubject> res = userSubjectRepo.findByUser(user.get());
        return res;
    }

    public Iterable<UserSubject> findAllUserSubjectBySubjectId(Long subjectId) {
        Optional<Subject> subject = subjectRepo.findById(subjectId);
        if(subject.isEmpty()) {
            return null;
        }

        Iterable<UserSubject> res = userSubjectRepo.findBySubject(subject.get());
        return res;
    }

}

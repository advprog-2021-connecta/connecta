package id.ac.ui.cs.advprog.connecta.notification.core;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import lombok.Data;

@Data
public class MessagesBooking {
    private String email;
    private String creator;
    private String event;
    private String participant;
    private String startTime;
    private String endTime;
    private String date;
    private String link;


    public MessagesBooking() {

    }

    public MessagesBooking(String email, Schedule schedule){
        this.email = email;
        this.creator = schedule.getEvent().getCreator().getFullName();
        this.participant = schedule.getParticipant().getFullName();
        this.startTime = schedule.getStartTime().toString();
        this.endTime = schedule.getEndTime().toString();
        this.event = schedule.getEvent().getTitle();
        this.date = schedule.getDate().toString();
        this.link = schedule.getEvent().getLink();
    }
}

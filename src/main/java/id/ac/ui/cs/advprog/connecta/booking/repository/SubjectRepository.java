package id.ac.ui.cs.advprog.connecta.booking.repository;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

    public Optional<Subject> findSubjectById(Long id);

}

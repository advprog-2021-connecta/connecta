package id.ac.ui.cs.advprog.connecta.scheduling.repository.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
@Builder
@Table(name = "schedules")
@AllArgsConstructor
@NoArgsConstructor
public class Schedule {

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "event")
    @JsonIgnore
    private Event event;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "date")
    private LocalDate date;

    @JsonFormat(pattern = "HH:mm")
    @Column(name = "start_time")
    private LocalTime startTime;

    @JsonFormat(pattern = "HH:mm")
    @Column(name = "end_time")
    private LocalTime endTime;

    @OneToOne
    @JoinColumn(name = "participant_id")
    private User participant;

    @Column(name = "is_available")
    private boolean isAvailable;

    public Schedule(Event event, LocalDate date, LocalTime startTime, LocalTime endTime) {
        this.event = event;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.isAvailable = true;
    }

}

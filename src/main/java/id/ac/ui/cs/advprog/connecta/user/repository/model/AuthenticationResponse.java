package id.ac.ui.cs.advprog.connecta.user.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class AuthenticationResponse implements Serializable {

    private final String jwt;

}

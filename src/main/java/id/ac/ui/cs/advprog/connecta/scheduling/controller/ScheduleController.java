package id.ac.ui.cs.advprog.connecta.scheduling.controller;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.scheduling.service.EventService;
import id.ac.ui.cs.advprog.connecta.scheduling.service.ScheduleService;
import id.ac.ui.cs.advprog.connecta.security.util.JwtUtil;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/scheduling/schedule")
public class ScheduleController {

    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createSchedule(@RequestBody Map<String, String> requestBody) {
        var dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        var timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        var event = eventService.getEventById(Long.parseLong(requestBody.get("eventId")));
        var date = LocalDate.parse(requestBody.get("date"), dateFormatter);
        var startTime = LocalTime.parse(requestBody.get("startTime"), timeFormatter);
        var endTime = LocalTime.parse(requestBody.get("endTime"), timeFormatter);
        
        String todayStr = dateFormatter.format(LocalDate.now());
        var today = LocalDate.parse(todayStr, dateFormatter);

        String currTimeStr = timeFormatter.format(LocalTime.now());
        var currTime = LocalTime.parse(currTimeStr, timeFormatter);

        if (date.isBefore(event.getStartDate()) || date.isAfter(event.getEndDate())) {
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Kesalahan pada tanggal.");
        } else if (date.isEqual(today) && startTime.isBefore(currTime) ||
                   endTime.isBefore(startTime)) {
                return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Kesalahan pada waktu.");
        }
        
        List<Schedule> eventSchedules = scheduleService.getSchedulesOfEvent(event.getId());

        for (Schedule schedule: eventSchedules) {
            if (startTime.isAfter(schedule.getStartTime()) && startTime.isBefore(schedule.getEndTime()) ||
                endTime.isAfter(schedule.getStartTime()) && endTime.isBefore(schedule.getEndTime()) ||
                startTime.equals(schedule.getStartTime()) || endTime.equals(schedule.getEndTime())) {
                    return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Kesalahan pada waktu.");
            }
        }

        var schedule = new Schedule(
            event,
            date,
            startTime,
            endTime
        );

        return ResponseEntity.ok(scheduleService.createSchedule(schedule));
    }

    @GetMapping(path = "/{eventId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Schedule>> getSchedulesOfEvent(@PathVariable(value = "eventId") Long eventId) {
        return ResponseEntity.ok(scheduleService.getSchedulesOfEvent(eventId));
    }

    @GetMapping(path = "/getAllSchedule", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Schedule>> getAllSchedule() {
        return ResponseEntity.ok(scheduleService.getAllSchedule());
    }

    @PutMapping(value="/{id}/book")
    public ResponseEntity<?> bookScheduleById(@PathVariable Long id, @RequestHeader(name="Authorization") String token) {
        String userEmail = JwtUtil.extractEmail(token.split("Bearer ", -1)[1]);
        scheduleService.enroll(userEmail, id);
        return ResponseEntity.ok("user successfully enrolled");
    }

}

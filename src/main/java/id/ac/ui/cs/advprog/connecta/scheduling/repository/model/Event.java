package id.ac.ui.cs.advprog.connecta.scheduling.repository.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Builder
@Table(name = "events")
@AllArgsConstructor
@NoArgsConstructor
public class Event implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "subject")
    private Subject subject;

    @ManyToOne
    @JoinColumn(name = "creator")
    private User creator;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "start_date")
    private LocalDate startDate;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "end_date")
    private LocalDate endDate;

    @OneToMany(mappedBy = "event")
    private List<Schedule> schedule;

    @Column(name = "link")
    private String link;

    public Event(String title, Subject subject, User creator, LocalDate startDate, LocalDate endDate, String link) {
        this.title = title;
        this.subject = subject;
        this.creator = creator;
        this.startDate = startDate;
        this.endDate = endDate;
        this.link = link;
    }
}

package id.ac.ui.cs.advprog.connecta.notification.service;

import id.ac.ui.cs.advprog.connecta.booking.model.Booking;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;

public interface NotificationService {
    void notifyNewEvent(Event event);
    void notifyNewBooking(Booking booking);
}

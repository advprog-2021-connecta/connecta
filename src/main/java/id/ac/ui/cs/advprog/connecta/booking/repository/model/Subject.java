package id.ac.ui.cs.advprog.connecta.booking.repository.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "subjects")
public class Subject implements Serializable {
    private @Id @GeneratedValue Long id;
    private String title;
    private String description;
    private Date createdAt;

    @Transient
    @JsonInclude
    private boolean hasEnrolled;

    @Transient
    @JsonInclude
    private Long enrollmentId;

    @Transient
    @JsonInclude
    private boolean isTeachingAssistant;

    public Subject(String title, String description) {
        this.title = title;
        this.description = description;
        this.createdAt = new Date();
        this.hasEnrolled = false;
        this.isTeachingAssistant = false;
    }
}

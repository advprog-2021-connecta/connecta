package id.ac.ui.cs.advprog.connecta.user.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "users")
@DiscriminatorValue("USER")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "email", nullable = false, unique = true)
    @NotNull
    private String email;

    @Column(name = "password", nullable = false)
    @JsonIgnore
    private String password;

    @Column(name = "is_admin", nullable = false,  columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean isAdmin;

    @CreatedDate
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @OneToMany(mappedBy = "creator")
    @JsonIgnore
    private List<Event> createdEvents;

    public User(String fullName, String email, String password) {
        this.fullName = fullName;
        this.email = email;
        this.password = new BCryptPasswordEncoder().encode(password);
        this.createdAt = new Date();
        this.isAdmin = false;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

}

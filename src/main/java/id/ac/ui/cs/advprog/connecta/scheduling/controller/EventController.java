package id.ac.ui.cs.advprog.connecta.scheduling.controller;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.booking.service.SubjectService;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.scheduling.service.EventService;
import id.ac.ui.cs.advprog.connecta.scheduling.service.ScheduleService;
import id.ac.ui.cs.advprog.connecta.security.util.JwtUtil;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/scheduling/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private ScheduleService scheduleService;
    
    @Autowired
    private UserService userService;

    @Autowired
    private SubjectService subjectService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> createEvent(@RequestBody Map<String, String> requestBody, @RequestHeader(name = "Authorization") String token) {
        String creatorEmail = JwtUtil.extractEmail(token.split("Bearer ", -1)[1]);
        var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        var subject = subjectService.getSubjectById(Long.parseLong(requestBody.get("subjectId")));
        var creator = userService.getUserByEmail(creatorEmail);
        var startDate = LocalDate.parse(requestBody.get("startDate"), formatter);
        var endDate = LocalDate.parse(requestBody.get("endDate"), formatter);
        String link = requestBody.get("link");

        String todayStr = formatter.format(LocalDate.now());
        var today = LocalDate.parse(todayStr, formatter);

        if (startDate.isBefore(today) || endDate.isBefore(startDate)) {
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Kesalahan pada tanggal.");
        }

        List<Event> userEvents = eventService.getEventsOfUser(creator);

        for (Event event: userEvents) {
            if (startDate.isAfter(event.getStartDate()) && startDate.isBefore(event.getEndDate()) ||
                endDate.isAfter(event.getStartDate()) && endDate.isBefore(event.getEndDate()) ||
                startDate.equals(event.getStartDate()) || endDate.equals(event.getEndDate())) {
                return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Kesalahan pada tanggal.");
            }
        }

        var event = new Event(
            requestBody.get("title"),
            subject,
            creator,
            startDate,
            endDate,
                link
        );

        return ResponseEntity.ok(eventService.createEvent(event));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getEvent(@PathVariable(value = "id") Long id) {
        var event = eventService.getEventById(id);

        if (event == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(event);
    }

    @GetMapping(path = "/geteventbyschedule/{eventId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getEventBySchedule(@PathVariable(value = "eventId") Long scheduleId) {
        var schedule = scheduleService.getScheduleById(scheduleId);
        
        return ResponseEntity.ok(eventService.getEventBySchedule(schedule));
    }

    @GetMapping(path = "/getusersevents", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Event>> getEventsOfUser(@RequestHeader(name = "Authorization") String token) {
        String userEmail = JwtUtil.extractEmail(token.split("Bearer ", -1)[1]);
        User user = userService.getUserByEmail(userEmail);
        
        return ResponseEntity.ok(eventService.getEventsOfUser(user));
    }
}

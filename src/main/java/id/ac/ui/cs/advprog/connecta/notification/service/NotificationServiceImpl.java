package id.ac.ui.cs.advprog.connecta.notification.service;

import id.ac.ui.cs.advprog.connecta.booking.model.Booking;
import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;
import id.ac.ui.cs.advprog.connecta.notification.config.RabbitMqConfig;
import id.ac.ui.cs.advprog.connecta.notification.core.MessagesBooking;
import id.ac.ui.cs.advprog.connecta.notification.core.MessagesEvent;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;
import id.ac.ui.cs.advprog.connecta.user.service.UserSubjectService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService{

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private UserSubjectService userSubjectService;

    public void notifyNewEvent(Event event)  {
        var subject = event.getSubject();
        for (UserSubject userSubject : userSubjectService.findAllUserSubjectBySubjectId(subject.getId())) {
            if (!userSubject.getIsTeachingAssistant()) {
                var user = userSubject.getUser();
                rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_NAME, RabbitMqConfig.KEY_1, new MessagesEvent(user.getEmail(), event));
            }
        }
    }

    public void notifyNewBooking(Booking booking){
        var schedule = booking.getJadwal();
        User creator = schedule.getEvent().getCreator();
        User participant = schedule.getParticipant();

        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_NAME, RabbitMqConfig.KEY_2,new MessagesBooking(creator.getEmail(), schedule));
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_NAME, RabbitMqConfig.KEY_2,new MessagesBooking(participant.getEmail(), schedule));
    }



}

package id.ac.ui.cs.advprog.connecta.booking.service;

import id.ac.ui.cs.advprog.connecta.booking.repository.model.Subject;

import java.util.List;

public interface SubjectService {
    public List<Subject> findAllEligibleSubjects(String email);
    public void enrollToSubject(Long subjectId, String email);
    public void unenrollFromSubject(Long subjectId);
    public Subject createSubject(Subject subject);
    public Subject getSubjectById(Long id);
}

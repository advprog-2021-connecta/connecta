package id.ac.ui.cs.advprog.connecta.user.repository.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthenticationRequest implements Serializable {

    private String email;
    private String password;

}

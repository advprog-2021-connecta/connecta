package id.ac.ui.cs.advprog.connecta.security.constant;

import io.jsonwebtoken.SignatureAlgorithm;

public class SecurityConstant {

    public static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    public static final String SECRET_KEY = "ELfng8OpLPjnHSeJ2k9YaqPgxRrNNFEx8/HBCsoq39I=";

    public static final int EXPIRATION_TIME = 1000 * 60 * 60 * 10; 

    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String HEADER_STRING = "Authorization";

}

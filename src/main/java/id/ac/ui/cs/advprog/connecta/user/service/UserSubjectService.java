package id.ac.ui.cs.advprog.connecta.user.service;

import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;

public interface UserSubjectService {
    public UserSubject updateUserSubject(UserSubject req);
    public Iterable<UserSubject> findAllUserSubjectByUserId(Long userId);
    public Iterable<UserSubject>  findAllUserSubjectBySubjectId(Long subjectId);
}

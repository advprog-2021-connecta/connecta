package id.ac.ui.cs.advprog.connecta.common.controller.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {
    private int code;
    private String description;
}

package id.ac.ui.cs.advprog.connecta.user.controller.models;

import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;
import lombok.Data;

@Data
public class UpdateUserSubjectRequest {
    private Boolean isTeachingAssistant;

    public UserSubject toModel() {
        var userSubject = new UserSubject();
        userSubject.setIsTeachingAssistant(isTeachingAssistant);
        return userSubject;
    }
}

package id.ac.ui.cs.advprog.connecta.user.controller;

import id.ac.ui.cs.advprog.connecta.common.controller.models.APIResponse;
import id.ac.ui.cs.advprog.connecta.common.controller.models.ErrorResponse;
import id.ac.ui.cs.advprog.connecta.user.controller.models.UpdateUserSubjectRequest;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import id.ac.ui.cs.advprog.connecta.user.repository.model.UserSubject;
import id.ac.ui.cs.advprog.connecta.user.service.UserService;
import id.ac.ui.cs.advprog.connecta.user.service.UserSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserSubjectService userSubjectSvc;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> createUser(@RequestBody Map<String, String> requestBody) {
        var user = new User(
                requestBody.get("fullName"),
                requestBody.get("email"),
                requestBody.get("password")
        );
        return ResponseEntity.ok(userService.createUser(user));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<User>> getListUser() {
        return ResponseEntity.ok(userService.getListUser());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> getUser(@PathVariable(value = "id") Long id) {
        var user = userService.getUserById(id);
        if (user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(user);
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> updateUser(@PathVariable(value = "id") Long id, @RequestBody Map<String, String> requestBody) {
        var newUser = new User(
            requestBody.get("fullName"),
            requestBody.get("email"),
            requestBody.get("password")
        );
        return ResponseEntity.ok(newUser);
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long id) {
        userService.deleteUserById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/{id}/userSubject",  produces = {"application/json"})
    public ResponseEntity<?> updateUserSubject(@PathVariable(value= "id") int id, @RequestBody UpdateUserSubjectRequest request) {
        Map<String, String> res = new HashMap<String, String>();
        try {
            var req = request.toModel();
            req.setId(Long.valueOf(id));
            UserSubject userSubject  = userSubjectSvc.updateUserSubject(req);
            
            if (userSubject == null) {
                ErrorResponse err = ErrorResponse
                    .builder()
                    .code(404)
                    .description("user not found")
                    .build();

                return ResponseEntity.status(404).body(APIResponse
                    .builder()
                    .error(err)
                    .build());
            }

            res.put("message", "user updated successfully");
            return ResponseEntity.ok(APIResponse.builder().data(res).build());
        } catch (Exception e) {
            ErrorResponse err = ErrorResponse
            .builder()
            .code(500)
            .description(e.toString())
            .build();

            return ResponseEntity.status(500).body(APIResponse
            .builder()
            .error(err)
            .build());
        }
    }

    @GetMapping(path = "/{id}/userSubject",  produces = {"application/json"})
    public ResponseEntity<?> findAllUserSubject(@PathVariable(value= "id") Long id) {
        Iterable<UserSubject> subjects = userSubjectSvc.findAllUserSubjectByUserId(id);
        return ResponseEntity.ok(subjects);
    }
}

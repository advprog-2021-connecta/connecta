package id.ac.ui.cs.advprog.connecta.booking.model;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import id.ac.ui.cs.advprog.connecta.user.repository.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Bookings")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm")
    private Date bookingDate;

    @Column(columnDefinition = "SMALLINT default 0")
    private int status; // e.g -1: canceled, 0: pending, 1: confirmed

    @ManyToOne(fetch = FetchType.LAZY)
    private User student;

    @ManyToOne(fetch = FetchType.LAZY)
    private User teachingAssistant;

    @ManyToOne(fetch = FetchType.LAZY)
    private Schedule jadwal;

}

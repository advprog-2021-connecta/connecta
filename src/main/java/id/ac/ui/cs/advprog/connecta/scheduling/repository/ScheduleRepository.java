package id.ac.ui.cs.advprog.connecta.scheduling.repository;

import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Event;
import id.ac.ui.cs.advprog.connecta.scheduling.repository.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    public Schedule findScheduleById(Long id);
    public Schedule findScheduleByEvent(Event event);
}

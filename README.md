Master Branch Pipeline:
[![pipeline status](https://gitlab.com/advprog-2021-connecta/connecta/badges/master/pipeline.svg)](https://gitlab.com/advprog-2021-connecta/connecta/-/commits/master)
[![coverage report](https://gitlab.com/advprog-2021-connecta/connecta/badges/master/coverage.svg)](https://gitlab.com/advprog-2021-connecta/connecta/-/commits/master)

Dev Branch Pipeline:
[![pipeline status](https://gitlab.com/advprog-2021-connecta/connecta/badges/develop/pipeline.svg)](https://gitlab.com/advprog-2021-connecta/connecta/-/commits/develop)
[![coverage report](https://gitlab.com/advprog-2021-connecta/connecta/badges/develop/coverage.svg)](https://gitlab.com/advprog-2021-connecta/connecta/-/commits/develop)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=alert_status)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=bugs)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=code_smells)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=ncloc)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=security_rating)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=sqale_index)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=advprog-2021-connecta_connecta&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=advprog-2021-connecta_connecta)